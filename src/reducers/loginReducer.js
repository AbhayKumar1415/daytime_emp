import { LOGIN_STATUS } from '../actions/types';

const initialState = {
  loginStatus: false
};
export default function(state = initialState, action) {
  switch (action.type) {
    case LOGIN_STATUS:
      return {
        ...state,
        loginStatus: action.payload
      };
    default:
      return state;
  }
}