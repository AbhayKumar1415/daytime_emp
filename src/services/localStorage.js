module.exports = {
  getUser: function() {
    return window.localStorage.getItem('user');
  },
  setUser: function(guser) {
    window.localStorage.setItem('user',JSON.stringify(guser));
  }
}
