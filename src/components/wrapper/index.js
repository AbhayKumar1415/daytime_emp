import React, { Component } from 'react';
import Team from '../team';
import Gems from '../gems';
import Profile from '../profile';
import Footer from '../footer';
import Home from '../home';
import Header from '../header';
import './wrapper.css';
class Wrapper extends Component {

  constructor() {
   super();
  }
  render() {
    return (
        <div>
          <Header />
          {/* <Home /> */}
           <Team />
          <Footer />
        </div>
    );
  }
}

export default Wrapper;
