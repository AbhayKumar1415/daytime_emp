import React,  {Component} from 'react';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Done from 'material-ui/svg-icons/action/done';
import FloatingActionButton from 'material-ui/FloatingActionButton';

// import MobileTearSheet from '../MobileTearSheet';
import './home.css';
class Home extends Component {
  constructor() {
    super();
   }
   state = {
    value: 1,
  };

  handleChange = (event, index, value) => this.setState({value});
   render() {
    return (
      <div className="home-wrapper">
        <div className="profile-view-wrapper">
          <div className="profile-user">
            <div className="profile-user-name">
              Abhay Kumar
            </div>
            <div className="profile-user-role">
              Javascript Developer
            </div>
          </div>
          <div className="profile-leave-wrapper">
            <div className="profile-leave">
              <div className="profile-leave-data">
                20
              </div>
              <div>
                WFH
              </div>
            </div>
            <div className="profile-wfh">
              <div className="profile-leave-data">
                20
              </div>
              <div>
                LEAVES
              </div>
            </div>
          </div>
        </div>
        <div className="attendance-wrapper">
          <div className="attendance-options">
            <SelectField
                floatingLabelText="Attendance"
                value={this.state.value}
                onChange={this.handleChange}
                className="attendance-select"
              >
                <MenuItem value={1} primaryText="Select" />
                <MenuItem value={2} primaryText="Office" />
                <MenuItem value={3} primaryText="W.F.H" />
                <MenuItem value={4} primaryText="Leave" />
                <MenuItem value={5} primaryText="Running late" />
              </SelectField>
          </div>
          <div className="attendance-button">
          <FloatingActionButton mini={true}>
            <Done />
          </FloatingActionButton>
          </div>
        </div>
      </div>
      );
  }
}

export default Home;