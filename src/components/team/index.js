import React, { Component } from 'react';
import './team.css';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import {List, ListItem} from 'material-ui/List';
import ActionGrade from 'material-ui/svg-icons/action/grade';
import Divider from 'material-ui/Divider';
import Avatar from 'material-ui/Avatar';
import {pinkA200, transparent} from 'material-ui/styles/colors';
import Snackbar from 'material-ui/Snackbar';

class Team extends Component {

  constructor(props) {
      super(props);
      this.state = {
        open: false,
      };
    }

    handleClick = () => {
      this.setState({
        open: true,
      });
    };

    handleRequestClose = () => {
      this.setState({
        open: false,
      });
    };

  render() {
    return (
      <div className="team-wrapper">
        <div className="team-search">
              <TextField
                hintText="Search"
                className="team-search-box"
                fullWidth={false}
              />
        </div>

        <div>
        <Snackbar
            open={this.state.open}
            message="Event added to your calendar"
            autoHideDuration={4000}
            onRequestClose={this.handleRequestClose}
          />
        </div>

          <div className="List">
            <List>
              <ListItem
                onClick={this.handleClick}
                primaryText="Chelsea Otakan"
                leftIcon={<ActionGrade color={pinkA200} />}
                rightAvatar={<Avatar src="images/chexee-128.jpg" />}
              />
              <ListItem
                onClick={this.handleClick}
                primaryText="Eric Hoffman"
                insetChildren={true}
                rightAvatar={<Avatar src="images/kolage-128.jpg" />}
              />
              <ListItem
                primaryText="James Anderson"
                insetChildren={true}
                rightAvatar={<Avatar src="images/jsa-128.jpg" />}
              />
              <ListItem
                primaryText="Kerem Suer"
                insetChildren={true}
                rightAvatar={<Avatar src="images/kerem-128.jpg" />}
              />
            </List>
            <Divider inset={true} />
            <List>
              <ListItem
                primaryText="Adelle Charles"
                leftAvatar={
                  <Avatar
                    color={pinkA200} backgroundColor={transparent}
                    style={{left: 8}}
                  >
                    A
                  </Avatar>
                }
                rightAvatar={<Avatar src="images/adellecharles-128.jpg" />}
              />
              <ListItem
                primaryText="Adham Dannaway"
                insetChildren={true}
                rightAvatar={<Avatar src="images/adhamdannaway-128.jpg" />}
              />
              <ListItem
                primaryText="Allison Grayce"
                insetChildren={true}
                rightAvatar={<Avatar src="images/allisongrayce-128.jpg" />}
              />
              <ListItem
                primaryText="Angel Ceballos"
                insetChildren={true}
                rightAvatar={<Avatar src="images/angelceballos-128.jpg" />}
              />
            </List>
            <Divider inset={true} />
            <List>
              <ListItem
                primaryText="Adelle Charles"
                leftAvatar={
                  <Avatar
                    color={pinkA200} backgroundColor={transparent}
                    style={{left: 8}}
                  >
                    B
                  </Avatar>
                }
                rightAvatar={<Avatar src="images/adellecharles-128.jpg" />}
              />
              <ListItem
                primaryText="Adham Dannaway"
                insetChildren={true}
                rightAvatar={<Avatar src="images/adhamdannaway-128.jpg" />}
              />
              <ListItem
                onClick={this.handleClick}
                primaryText="Allison Grayce"
                insetChildren={true}
                rightAvatar={<Avatar src="images/allisongrayce-128.jpg" />}
              />
              <ListItem
                onClick={this.handleClick}
                primaryText="Angel Ceballos"
                insetChildren={true}
                rightAvatar={<Avatar src="images/angelceballos-128.jpg" />}
              />
            </List>
          </div>
      </div>
    );
  }
}

export default Team;
