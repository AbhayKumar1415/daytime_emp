import React, { Component } from 'react';
import './login.css';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import RaisedButton from 'material-ui/RaisedButton';
import logoPNG from '../../assets/logo.png';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { login } from '../../actions/login';
// import localStorage from '../../services/localStorage';
import localStorage from '../../services/localStorage';
class Login extends Component {
  
  constructor(props) {
   super(props);
   this.googleInit();
  }
  
  googleInit() {
    window.gapi.load('auth2', () => {
      window.auth2 = window.gapi.auth2.init({
        client_id: '377664800668-9vdmc9sj27pr22ucqrs5qsn8cd3mf76c.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
        scope: 'profile email'
      });
      this.attachSignin(document.getElementById('googleBtn'));
    });
  }
  attachSignin(element) {
    window.auth2.attachClickHandler(element, {},
      (googleUser) => {
        let profile = googleUser.getBasicProfile();
        let gUser = {email: profile.getEmail(), img: profile.getImageUrl()};
        console.log('guser :',gUser);
        // this.login(gUser);
        localStorage.setUser(gUser);
        this.updateLoginStatus();
      }, (error) => {
        alert(JSON.stringify(error, undefined, 2));
      });
  }
  updateLoginStatus() {
    this.props.updateLoginStatus();
    this.props.login();
  }
  render() {
    return (
      <div className="Login">
        <img src={logoPNG} alt="" className="logo"/>
        <RaisedButton label="Login" id="googleBtn" />
      </div>
    );
  }
}

// export default Login;
export default connect(null, { login })(Login);
