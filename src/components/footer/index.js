import React, {Component} from 'react';
import FontIcon from 'material-ui/FontIcon';
import {BottomNavigation, BottomNavigationItem} from 'material-ui/BottomNavigation';
import Paper from 'material-ui/Paper';
import './footer.css';
const home = <FontIcon className="material-icons">home</FontIcon>;
const gem = <FontIcon className="material-icons">star</FontIcon>;
const team = <FontIcon className="material-icons">people</FontIcon>;
const profile = <FontIcon className="material-icons">person</FontIcon>;
class Footer extends Component {
  state = {
    selectedIndex: 0,
  };

  select = (index) => this.setState({selectedIndex: index});

  render() {
    return (
      <div className="footer-wrapper">
        <Paper zDepth={1}>
          <BottomNavigation selectedIndex={this.state.selectedIndex}>
            <BottomNavigationItem
              icon={home}
              onClick={() => this.select(0)}
            />
            <BottomNavigationItem
              icon={gem}
              onClick={() => this.select(1)}
            />
            <BottomNavigationItem
              icon={team}
              onClick={() => this.select(2)}
            />
            <BottomNavigationItem
              icon={profile}
              onClick={() => this.select(3)}
            />
          </BottomNavigation>
        </Paper>
      </div>
    );
  }
}

export default Footer;