import { LOGIN_STATUS } from './types.js';
import { URL } from '../constants/url';

export const login = () => dispatch => {
  console.log('Base Url :',URL.BASE_URL);
  console.log('login route :',URL.LOGIN);
  // return 'Success';
  dispatch({
    type: LOGIN_STATUS,
    payload: true
  })
}