import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Login from './components/login';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Wrapper from './components/wrapper';
import { Provider } from 'react-redux';
import store from './store';
import localStorage from '../src/services/localStorage'

class App extends Component {
  constructor() {
    super();
    this.state = {
      loginState: false
    }
  }
  componentWillMount() {
    console.log('component will mount');
    let x = JSON.parse(localStorage.getUser());
    if(x) {
      this.updateLoginStatus();
    }
    console.log('User :',x);
  }
  updateLoginStatus() {
    this.setState({
      loginState: true
    });
  }

  greeting(props) {
    console.log('Greetings props :',props);
    const isLoggedIn = props;
    if (!isLoggedIn) {
      return <Login updateLoginStatus={this.updateLoginStatus.bind(this)} /> ;
    }
    return <Wrapper />;
  }

  render() {
    return (
      <Provider store={store}>
        <MuiThemeProvider>
          {this.greeting(this.state.loginState)}
        </MuiThemeProvider>
      </Provider>
    );
  }
}

export default App;
